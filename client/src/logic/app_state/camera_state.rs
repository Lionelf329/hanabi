use crate::constants::layout::THETA;
use crate::logic::{Animation, Perspective};
use graphics::{Camera, Point, Vector};

pub struct CameraState {
    pub perspective: Perspective,
    pub u: Animation<Vector>,
    pub e: Animation<Point>,
    pub g: Animation<Point>,
}

impl CameraState {
    pub fn new(perspective: Perspective) -> CameraState {
        let (u, e, g) = perspective.camera();
        CameraState {
            perspective,
            u: Animation::Fixed(u),
            e: Animation::Fixed(e),
            g: Animation::Fixed(g),
        }
    }

    pub fn change_perspective(&mut self, perspective: Perspective) {
        let (u, e, g) = perspective.camera();
        self.u.animate_to(u, None);
        self.e.animate_to(e, None);
        self.g.animate_to(g, None);
        self.perspective = perspective;
    }

    pub fn tick(&mut self) -> bool {
        let u = self.u.tick();
        let e = self.e.tick();
        let g = self.g.tick();
        u || e || g
    }

    pub fn get_camera(&self, width: u32, height: u32) -> Camera {
        Camera::new(
            self.u.position(),
            self.e.position(),
            self.g.position(),
            width,
            height,
            THETA.to_radians(),
        )
    }
}
