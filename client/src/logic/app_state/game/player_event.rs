use crate::logic::Animation;
use graphics::Matrix;
use hanabi_shared::Hint;

pub enum PlayerEvent {
    None,
    Disconnected,
    Action(usize, PlayCardOutcome),
    PlayCard {
        card: usize,
        animation: Animation<Matrix>,
        will_draw: bool,
        discard: bool,
    },
    DrawCard {
        animation: Animation<f64>,
    },
    ReceiveHint(Hint),
}

impl PlayerEvent {
    pub fn tick(&mut self) -> bool {
        match self {
            PlayerEvent::PlayCard { animation, .. } => animation.tick(),
            PlayerEvent::DrawCard { animation, .. } => animation.tick(),
            PlayerEvent::Action(..) => false,
            PlayerEvent::ReceiveHint(_) => false,
            PlayerEvent::Disconnected => false,
            PlayerEvent::None => false,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum PlayCardOutcome {
    Cancel,
    Play,
    Discard,
}

impl PlayCardOutcome {
    pub fn color(&self) -> usize {
        match self {
            PlayCardOutcome::Play => 3,    // green
            PlayCardOutcome::Discard => 1, // red
            PlayCardOutcome::Cancel => 4,  // blue
        }
    }
}
