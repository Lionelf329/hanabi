use crate::constants::svg;
use crate::logic::{actions, Animation};
use crate::render::table_card_transform;
use crate::render::transform::{base_card_transform, base_player_transform};
use graphics::transform::translate_z;
use hanabi_shared::constants::{CODE_LENGTH, MAX_HINTS, STACK_SIZE};
use hanabi_shared::{Card, Hint};
pub use player::*;
pub use player_event::*;

mod player;
mod player_event;

pub struct Game {
    pub code: [usize; CODE_LENGTH],
    pub me: usize,
    pub remaining: usize,
    pub players: Vec<Player>,
    pub discarded: Vec<Card>,
    pub stacks: Vec<usize>,
    pub hints: usize,
    pub fuses: usize,
    pub turn: usize,
}

impl Game {
    pub fn play(&mut self, card_index: usize, new_card: Option<Card>, discard: bool) {
        let player_index = self.turn;
        let player_count = self.players.len();
        let card_count = self.players[player_index].cards.len();
        let start = {
            let t1 = base_card_transform(card_index as f64, card_count as f64, false);
            let t2 = base_player_transform(player_index, player_count);
            t2 * t1
        };
        let end = if discard {
            translate_z(-svg::SIZE).transform(start)
        } else {
            table_card_transform(
                self.players[player_index].cards[card_index],
                self.me,
                player_count,
                self.stacks.len(),
            )
        };
        let on_complete = actions::on_complete_play(player_index, card_index, new_card, discard);
        self.players[self.turn].event = PlayerEvent::PlayCard {
            card: card_index,
            animation: Animation::animating(start, end, on_complete),
            will_draw: new_card.is_some(),
            discard,
        };
        self.next_turn();
    }

    pub fn play_2(
        &mut self,
        player_index: usize,
        card_index: usize,
        new_card: Option<Card>,
        discard: bool,
    ) {
        let card = self.players[player_index].cards.remove(card_index);
        if discard {
            self.discarded.push(card);
            self.add_hint();
        } else {
            let i = card.color - 1;
            if self.stacks[i] + 1 == card.number {
                self.stacks[i] += 1;
                if self.stacks[i] == STACK_SIZE {
                    self.add_hint();
                }
            } else {
                self.discarded.push(card);
                self.fuses -= 1;
            }
        }
        self.players[player_index].event = match new_card {
            Some(card) => {
                if self.remaining > 0 {
                    self.remaining -= 1;
                }
                self.players[player_index].cards.push(card);
                PlayerEvent::DrawCard {
                    animation: Animation::Animating {
                        start: svg::SIZE,
                        end: 0.0,
                        progress: 0,
                        on_complete: actions::on_complete_draw(player_index),
                    },
                }
            }
            None => PlayerEvent::None,
        };
    }

    pub fn hint(&mut self, player_index: usize, hint: Hint) {
        for card in &mut self.players[player_index].cards {
            card.hint(hint);
        }
        self.players[player_index].event = PlayerEvent::ReceiveHint(hint);
        if self.hints > 0 {
            self.hints -= 1;
        }
        self.next_turn();
    }

    fn add_hint(&mut self) {
        if self.hints < MAX_HINTS {
            self.hints += 1;
        }
    }

    fn next_turn(&mut self) {
        self.turn = (self.turn + 1) % self.players.len();
    }

    pub fn tick(&mut self) -> bool {
        let mut updated = false;
        for p in &mut self.players {
            updated |= p.event.tick();
        }
        updated
    }
}
