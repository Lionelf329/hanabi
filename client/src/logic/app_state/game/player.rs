use crate::logic::PlayerEvent;
use hanabi_shared::constants::STACK_SIZE;
use hanabi_shared::{Card, Hint};

pub struct Player {
    pub event: PlayerEvent,
    pub cards: Vec<Card>,
}

impl Player {
    pub fn new(cards: Vec<Card>) -> Player {
        Player {
            event: PlayerEvent::None,
            cards,
        }
    }

    pub fn find_relevant_hints(&self) -> Vec<Hint> {
        let mut numbers = [false; STACK_SIZE];
        let mut colors = [false; 5];
        for card in &self.cards {
            numbers[card.number - 1] = true;
            if card.color == 6 {
                colors = [true; 5];
            } else {
                colors[card.color - 1] = true;
            }
        }
        let mut hints = Vec::new();
        for (i, h) in numbers.iter().enumerate() {
            if *h {
                hints.push(Hint::Number(i + 1));
            }
        }
        for (i, h) in colors.iter().enumerate() {
            if *h {
                hints.push(Hint::Color(i + 1));
            }
        }
        hints
    }
}
