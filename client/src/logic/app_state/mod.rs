pub use animation::*;
pub use camera_state::CameraState;
pub use game::*;
pub use global::*;
use hanabi_shared::constants::CODE_LENGTH;
pub use perspective::*;

mod animation;
mod camera_state;
mod game;
mod global;
mod perspective;

pub enum State {
    InitialLoad,
    Host,
    Join([usize; CODE_LENGTH]),
    Joined {
        code: [usize; CODE_LENGTH],
        players: Vec<bool>,
        me: Option<usize>,
    },
    Game {
        camera: CameraState,
        game: Game,
    },
}

impl State {
    pub fn tick(&mut self) -> bool {
        if let State::Game { camera, game } = self {
            let t1 = camera.tick();
            let t2 = game.tick();
            t1 || t2
        } else {
            false
        }
    }
}
