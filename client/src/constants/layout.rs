pub const DISCARDED_SIZE: f64 = 20.0;
pub const DISCARDED_COL_LEN: usize = 12;

pub const HINT_FACTOR: f64 = 2.0;
pub const HINT_PADDING: f64 = 0.4;

// Units
const INCHES: f64 = 10.0;
const FEET: f64 = INCHES * 12.0;

// Scene parameters
pub const THETA: f64 = 60.0_f64;
pub const TABLE_HEIGHT: f64 = 0.0;
pub const TABLE_RADIUS: f64 = 2.0 * FEET;
pub const VIEW_HEIGHT: f64 = 1.0 * FEET;
pub const OVERVIEW_HEIGHT: f64 = 2.0 * FEET;
pub const CARD_HEIGHT: f64 = 2.0 * INCHES;
pub const DISTANCE_PLAYER_TO_CARD: f64 = 0.8 * FEET;
pub const DISTANCE_PLAYER_TO_TABLE: f64 = 0.8 * FEET;
pub const CARD_GROW_FACTOR: f64 = 1.5;
pub const OVERLAY_FACTOR: f64 = 1.0;
