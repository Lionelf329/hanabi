pub mod color;
pub mod layout;
pub mod svg;

// Canvas values
pub const CANVAS_FACTOR: f64 = 1.5;
pub const CANVAS_ID: &str = "canvas";

// Animation parameters
pub const ANIMATION_STEP_COUNT: usize = 25;
pub const ANIMATION_STEP_DURATION: i32 = 20;

pub mod events {
    pub const RESIZE: &str = "resize";
    pub const MOUSE_DOWN: &str = "mousedown";
    pub const MOUSE_MOVE: &str = "mousemove";
    pub const MOUSE_UP: &str = "mouseup";
    pub const TOUCH_START: &str = "touchstart";
    pub const TOUCH_MOVE: &str = "touchmove";
    pub const TOUCH_END: &str = "touchend";
}
