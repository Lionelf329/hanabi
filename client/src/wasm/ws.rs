use std::collections::HashMap;
use std::sync::atomic::{AtomicUsize, Ordering};
use wasm_bindgen::prelude::{Closure, JsValue};
use wasm_bindgen::JsCast;
use web_sys::{ErrorEvent, MessageEvent, WebSocket};

static UNIQUE_ID: AtomicUsize = AtomicUsize::new(0);

static mut WEBSOCKETS: Option<HashMap<usize, WebSocketHandle>> = None;

#[allow(dead_code)]
struct WebSocketHandle {
    ws: WebSocket,
    open_callback: Closure<dyn Fn(wasm_bindgen::JsValue)>,
    message_callback: Closure<dyn Fn(MessageEvent)>,
    error_callback: Closure<dyn Fn(ErrorEvent)>,
}

impl WebSocketHandle {
    fn send(&self, message: &str) -> bool {
        if self.ws.ready_state() == WebSocket::OPEN {
            self.ws.send_with_str(message).is_ok()
        } else {
            false
        }
    }
}

impl Drop for WebSocketHandle {
    fn drop(&mut self) {
        self.ws.close().unwrap();
    }
}

fn websockets() -> &'static mut HashMap<usize, WebSocketHandle> {
    unsafe {
        if WEBSOCKETS.is_none() {
            WEBSOCKETS = Some(HashMap::new());
        }
        (&mut WEBSOCKETS).as_mut().unwrap()
    }
}

pub fn start(
    url: &str,
    open: Box<dyn Fn(&WebSocket)>,
    receive: Box<dyn Fn(String)>,
    error: Box<dyn Fn(ErrorEvent)>,
) -> usize {
    // create a websocket
    let ws: WebSocket = WebSocket::new(url).unwrap();

    // clone it, so the clone can be given to the "open" handler, then create a closure wrapping
    // each handler
    let cloned_ws = ws.clone();
    let open_callback = Closure::wrap(Box::new(move |_| open(&cloned_ws)) as Box<dyn Fn(JsValue)>);
    let message_callback = Closure::wrap(Box::new(move |e: MessageEvent| {
        let response = e.data().as_string().unwrap();
        receive(response);
    }) as Box<dyn Fn(MessageEvent)>);
    let error_callback = Closure::wrap(error);

    // set the closures as the handlers on the websocket object
    ws.set_onopen(Some(open_callback.as_ref().unchecked_ref()));
    ws.set_onmessage(Some(message_callback.as_ref().unchecked_ref()));
    ws.set_onerror(Some(error_callback.as_ref().unchecked_ref()));

    // get an ID that can be used to close it later, or to send things over it
    let id = UNIQUE_ID.fetch_add(1, Ordering::Relaxed);
    let handle = WebSocketHandle {
        ws,
        open_callback,
        message_callback,
        error_callback,
    };
    // store the websocket object in static memory to protect it from being dropped
    websockets().insert(id, handle);
    id
}

pub fn send(ws: usize, message: &str) -> bool {
    match websockets().get(&ws) {
        Some(ws) => ws.send(message),
        None => false,
    }
}

pub fn close(ws: usize) -> bool {
    websockets().remove(&ws).is_some()
}
