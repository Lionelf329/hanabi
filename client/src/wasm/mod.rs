pub use callbacks::*;
pub use canvas::*;
pub use ws::*;

mod callbacks;
mod canvas;
mod ws;

pub fn dimensions() -> (u32, u32) {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();
    (body.client_width() as u32, body.client_height() as u32)
}
