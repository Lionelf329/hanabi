use std::collections::HashMap;
use std::sync::atomic::{AtomicUsize, Ordering};
use wasm_bindgen::closure::{Closure, WasmClosure};
use wasm_bindgen::JsCast;

static UNIQUE_ID: AtomicUsize = AtomicUsize::new(0);

static mut CALLBACKS: Option<HashMap<usize, CallbackHandle>> = None;

/// This struct is stored in the CALLBACKS static variable, to make sure the callback is not
/// dropped. unregister is a function that will remove the event listener, and it is called
/// when the struct is dropped.
struct CallbackHandle {
    event_type: &'static str,
    unregister: Box<dyn FnMut()>,
}

impl Drop for CallbackHandle {
    fn drop(&mut self) {
        (self.unregister)();
    }
}

fn callbacks() -> &'static mut HashMap<usize, CallbackHandle> {
    unsafe {
        if CALLBACKS.is_none() {
            CALLBACKS = Some(HashMap::new());
        }
        (&mut CALLBACKS).as_mut().unwrap()
    }
}

pub fn add_callback(
    function: Box<impl ?Sized + WasmClosure + 'static>,
    element: &(impl Clone + Into<web_sys::EventTarget>),
    event_type: &'static str,
) -> usize {
    // create a JS closure
    let closure = Closure::wrap(function);

    // copy the element so it can be stored in the struct
    let element: web_sys::EventTarget = element.clone().into();

    // add the event listener
    element
        .add_event_listener_with_callback(event_type, closure.as_ref().unchecked_ref())
        .unwrap();

    // get a unique id that can be used to unregister the handler
    let id = UNIQUE_ID.fetch_add(1, Ordering::Relaxed);

    // create a boxed closure that will unregister the handler. This takes ownership of the
    // element, and the closure that is being used as an event listener.
    let unregister = Box::new(move || {
        element
            .remove_event_listener_with_callback(event_type, closure.as_ref().unchecked_ref())
            .unwrap();
    });

    // store the unregister closure along with the type of event in static memory, so it won't
    // be dropped.
    callbacks().insert(
        id,
        CallbackHandle {
            event_type,
            unregister,
        },
    );
    id
}

pub fn remove_callback(id: usize) {
    // get the static collection of callbacks and try to remove the element
    let cb = callbacks();
    if cb.remove_entry(&id).is_none() {
        // if the element wasn't there, print a list of entries
        if cb.len() > 0 {
            let mut result = String::from("Currently registered callbacks:");
            for (&id, c) in cb {
                result += &format!("\n{} - {}", id, c.event_type);
            }
            console_log!("{}", &result);
        } else {
            console_log!("no callbacks registered");
        }
    }
}

pub fn set_interval(function: Box<dyn Fn()>, interval: i32) -> usize {
    // create a JS closure
    let closure = Closure::wrap(function);

    // set the callback to be called on the specified interval
    let window = web_sys::window().unwrap();
    let interval = window
        .set_interval_with_callback_and_timeout_and_arguments_0(
            closure.as_ref().unchecked_ref(),
            interval,
        )
        .unwrap();

    // get the ID for unregistering it
    let id = UNIQUE_ID.fetch_add(1, Ordering::Relaxed);

    // create the callback to clear the interval
    let unregister = Box::new(move || {
        &closure;
        window.clear_interval_with_handle(interval);
    });

    // store it in static memory so it won't be dropped
    callbacks().insert(
        id,
        CallbackHandle {
            event_type: "timer",
            unregister,
        },
    );
    id
}

static mut TIMERS: Option<HashMap<usize, TimeoutHandle>> = None;

// the warning is suppressed because the linter doesn't like that I don't use the closure field
#[allow(dead_code)]
struct TimeoutHandle {
    timer_id: i32,
    function: Box<dyn Fn()>,
    closure: wasm_bindgen::closure::Closure<dyn Fn()>,
}

fn timers() -> &'static mut HashMap<usize, TimeoutHandle> {
    unsafe {
        if TIMERS.is_none() {
            TIMERS = Some(HashMap::new());
        }
        (&mut TIMERS).as_mut().unwrap()
    }
}

pub fn add_timer(function: Box<dyn Fn()>, timer: i32) -> usize {
    // get the ID for cancelling it
    let id = UNIQUE_ID.fetch_add(1, Ordering::Relaxed);

    // create a function that calls the function passed in, and then drops it. This would not be
    // valid if it were called now, because it hasn't been added to TIMERS yet
    let closure: Box<dyn Fn()> = Box::new(move || (_remove_timer(id, false).unwrap().function)());

    // wrap it as a JS closure
    let closure = Closure::wrap(closure);

    // JS call to set timeout
    let window = web_sys::window().unwrap();
    let timer_id = window
        .set_timeout_with_callback_and_timeout_and_arguments_0(
            closure.as_ref().unchecked_ref(),
            timer,
        )
        .unwrap();

    // store the callback in static memory
    timers().insert(
        id,
        TimeoutHandle {
            timer_id,
            function,
            closure,
        },
    );
    id
}

pub fn remove_timer(id: usize) {
    _remove_timer(id, true);
}

fn _remove_timer(id: usize, unregister: bool) -> Option<TimeoutHandle> {
    let timers = timers();
    match timers.remove(&id) {
        Some(handle) => {
            // if it was found, make the JS call to unregister it if necessary
            if unregister {
                let window = web_sys::window().unwrap();
                window.clear_timeout_with_handle(handle.timer_id);
            }
            Some(handle)
        }
        None => {
            // if the timer wasn't found, print a list of them
            if timers.len() > 0 {
                let mut result = String::from("Currently registered timers:");
                for &id in timers.keys() {
                    result += &format!(" {}", id);
                }
                console_log!("{}", &result);
            } else {
                console_log!("no timers registered");
            }
            None
        }
    }
}
