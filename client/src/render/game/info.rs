use crate::constants::{color, layout::OVERLAY_FACTOR, svg};
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::Matrix;
use hanabi_shared::constants::CODE_LENGTH;

pub fn draw_info<T: Copy>(
    scene: &mut Scene<T>,
    code: [usize; CODE_LENGTH],
    hints: usize,
    fuses: usize,
) {
    for i in 0..CODE_LENGTH {
        let t = icon_transform(i, 0);
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::CARD[code[i]], color::HOST),
            t,
            use_camera: false,
            on_click: None,
            category: None,
        });
    }
    for i in 0..3 {
        let color = if i < 3 - fuses {
            color::FUSE_RED
        } else {
            color::FUSE_GREY
        };
        let t = icon_transform(i + CODE_LENGTH, 0);
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::FUSE, color),
            t,
            use_camera: false,
            on_click: None,
            category: None,
        });
    }
    for i in 0..8 {
        let color = if i < hints {
            color::HINT_BLUE
        } else {
            color::HINT_GREY
        };
        let t = icon_transform(i, 1);
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::HINT, color),
            t,
            use_camera: false,
            on_click: None,
            category: None,
        });
    }
}

fn icon_transform(x: usize, y: usize) -> Matrix {
    let s = OVERLAY_FACTOR * svg::SIZE;
    Matrix::new([
        [OVERLAY_FACTOR, 0.0, 0.0, s * x as f64],
        [0.0, OVERLAY_FACTOR, 0.0, s * y as f64],
        [0.0, 0.0, 1.0, 0.0],
    ])
}
