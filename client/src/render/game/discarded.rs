use crate::constants::{color, layout, svg};
use crate::logic::{PlayCardOutcome, PlayerEvent};
use crate::render::render_card::RenderCard;
use crate::render::Category;
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::transform::{scale, scale_all};
use graphics::Matrix;
use hanabi_shared::Card;

pub fn draw_discarded(
    scene: &mut Scene<Category>,
    event: &PlayerEvent,
    cards: &Vec<Card>,
    w: u32,
    h: u32,
) {
    let w = w as f64;
    let h = h as f64;
    if let PlayerEvent::Action(_, outcome) = event {
        let t = scale(w * 0.1 / svg::SIZE, w * 0.1 / svg::SIZE, 1.0).translate(
            w * 0.9,
            h - w * 0.1,
            0.0,
        );
        let color = if let PlayCardOutcome::Discard = outcome {
            color::HINT_BLUE
        } else {
            color::GREY
        };
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::DISCARD, color),
            t,
            use_camera: false,
            on_click: None,
            category: Some(Category::Discard),
        });
    } else {
        for i in 0..cards.len() {
            let t = discard_transform(w, h, i);
            scene.push(Entity::Svg {
                svg: Svg::solid(&svg::CARD_BACKGROUND, "black"),
                t,
                use_camera: false,
                on_click: None,
                category: None,
            });
            scene.push(Entity::Svg {
                svg: cards[i].svg(false),
                t,
                use_camera: false,
                on_click: None,
                category: None,
            });
        }
    }
}

fn discard_transform(w: f64, h: f64, i: usize) -> Matrix {
    let r = (i % layout::DISCARDED_COL_LEN) as f64;
    let c = (i / layout::DISCARDED_COL_LEN) as f64;
    let x = w - (c + 1.0) * layout::DISCARDED_SIZE;
    let y = h - (r + 1.0) * layout::DISCARDED_SIZE;
    scale_all(layout::DISCARDED_SIZE / svg::SIZE).translate(x, y, 0.0)
}
