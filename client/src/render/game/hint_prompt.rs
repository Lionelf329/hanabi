use crate::constants::{color::card_color_svg, layout, svg};
use crate::logic::{actions, Player};
use crate::scene::{Entity, Scene};
use graphics::transform::scale_all;
use graphics::Matrix;
use hanabi_shared::Hint;

pub fn draw_hint_prompt<T: Copy>(
    scene: &mut Scene<T>,
    w: u32,
    player_index: usize,
    player: &Player,
) {
    let hints = player.find_relevant_hints();
    for (i, h) in hints.iter().enumerate() {
        let svg = match h {
            Hint::Color(c) => card_color_svg(&svg::CIRCLE, *c, false),
            Hint::Number(n) => card_color_svg(&svg::CARD[*n], 0, false),
        };
        scene.push(Entity::Svg {
            svg,
            t: hint_prompt_transform(i, hints.len(), w),
            use_camera: false,
            on_click: actions::hint(player_index, *h),
            category: None,
        })
    }
}

fn hint_prompt_transform(i: usize, hint_count: usize, w: u32) -> Matrix {
    let i = i as f64;
    let hint_count = hint_count as f64;
    let w = w as f64;
    let width = layout::HINT_FACTOR * svg::SIZE * (1.0 + layout::HINT_PADDING);
    let inset = layout::HINT_FACTOR * svg::SIZE * layout::HINT_PADDING / 2.0;
    scale_all(layout::HINT_FACTOR).translate_x(w / 2.0 + width * (i - hint_count / 2.0) + inset)
}
