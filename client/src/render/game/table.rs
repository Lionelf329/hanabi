use crate::constants::color::{TABLE_MATERIAL, TABLE_MESH};
use crate::constants::{layout, svg};
use crate::logic::actions::click_table;
use crate::render::render_card::RenderCard;
use crate::render::{transform, Category};
use crate::scene::{Entity, Scene};
use graphics::transform::{scale_all, translate};
use graphics::{Matrix, Object};
use hanabi_shared::constants::STACK_SIZE;
use hanabi_shared::Card;
use std::f64::consts::PI;

pub fn draw_table(
    scene: &mut Scene<Category>,
    stacks: &Vec<usize>,
    me: usize,
    player_count: usize,
) {
    scene.push(Entity::Shape(Object::new(
        TABLE_MESH,
        TABLE_MATERIAL,
        scale_all(layout::TABLE_RADIUS).transform(transform::base_table_transform()),
    )));
    let stack_count = stacks.len();
    for (i, &size) in stacks.iter().enumerate() {
        for j in 0..STACK_SIZE {
            let (color, number) = if j < size { (i + 1, j + 1) } else { (0, j + 1) };
            let card = Card::new(i + 1, j + 1);
            let t = table_card_transform(card, me, player_count, stack_count);
            let card = Card::new(color, number);
            card.add_to_scene(
                scene,
                t,
                click_table(me, player_count),
                false,
                Some(Category::Table),
            );
        }
    }
}

pub fn table_card_transform(
    card: Card,
    me: usize,
    player_count: usize,
    stack_count: usize,
) -> Matrix {
    let x = svg::SIZE * ((card.color - 1) as f64 - stack_count as f64 / 2.0);
    let y = svg::SIZE * (card.number as f64 - STACK_SIZE as f64 / 2.0 - 1.0);
    translate(x, y, 0.0)
        .scale_all(layout::CARD_GROW_FACTOR)
        .transform(transform::base_table_transform())
        .rotate_y(me as f64 / player_count as f64 * 2.0 * PI)
}
