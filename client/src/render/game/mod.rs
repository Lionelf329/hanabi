use crate::logic::{actions, CameraState, Game, Perspective, PlayerEvent};
use crate::render::Category;
use crate::scene::{Entity, Scene};
use graphics::{LightSource, Point};
pub use table::table_card_transform;

mod deck;
mod discarded;
mod hint_info;
mod hint_prompt;
mod info;
mod players;
mod table;

const LIGHT: LightSource = LightSource {
    position: Point::new(0.0, 360.0, 0.0),
    intensity: [1.0, 1.0, 1.0],
};

pub fn draw_game(w: u32, h: u32, camera: &CameraState, game: &Game) -> Scene<Category> {
    let me = game.me;
    let perspective = camera.perspective;
    let camera = camera.get_camera(w, h);
    let mut scene = Scene::new(Some(camera), Some(LIGHT));

    table::draw_table(&mut scene, &game.stacks, me, game.players.len());

    players::draw_players(&mut scene, &game.players, me, game.turn);
    info::draw_info(&mut scene, game.code, game.hints, game.fuses);
    discarded::draw_discarded(
        &mut scene,
        &game.players[game.me].event,
        &game.discarded,
        w,
        h,
    );
    deck::draw_deck(&mut scene, game.remaining, h);
    if let Perspective::Player { player, .. } = perspective {
        hint_prompt::draw_hint_prompt(&mut scene, w, player, &game.players[player]);
    } else if let PlayerEvent::Action(i, _) = game.players[game.me].event {
        let card = game.players[game.me].cards[i];
        hint_info::draw_hint_info(&mut scene, w, card);
    }
    let w = w as f64;
    let h = h as f64;
    let player_count = game.players.len();
    scene.push(Entity::BoundingBox(
        [[0.0, h * 0.75], [w, h * 0.75], [w, h], [0.0, h]],
        Some(actions::click_main(me, player_count)),
        None,
    ));
    scene
}
