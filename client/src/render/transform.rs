use crate::constants::{layout, svg};
use graphics::transform::{rotate, scale_all, translate, translate_x, translate_z};
use graphics::{Matrix, Vector};
use std::f64::consts::PI;

pub fn radial(radius: f64, angle: f64) -> Matrix {
    translate_z(-radius).rotate(Vector::new(0.0, 1.0, 0.0), angle * PI * 2.0)
}

pub fn base_player_transform(player: usize, player_count: usize) -> Matrix {
    let radius = layout::TABLE_RADIUS + layout::DISTANCE_PLAYER_TO_TABLE;
    let angle = player as f64 / player_count as f64;
    radial(radius, angle)
}

pub fn base_card_transform(card_index: f64, card_count: f64, flip: bool) -> Matrix {
    // center it on the origin
    let t1 = translate(-svg::SIZE / 2.0, -svg::SIZE / 2.0, 0.0);

    // offset it based on where it is in their hand
    let t2 = translate_x((card_index - (card_count - 1.0) / 2.0) * svg::SIZE);

    // make it bigger
    let t3 = scale_all(layout::CARD_GROW_FACTOR);

    // move forward and up, to where they would hold it
    let t4 = translate(0.0, layout::CARD_HEIGHT, layout::DISTANCE_PLAYER_TO_CARD);

    if flip {
        t1.scale_x(-1.0).transform(t2).transform(t3).transform(t4)
    } else {
        t1.transform(t2).transform(t3).transform(t4)
    }
}

pub fn base_table_transform() -> Matrix {
    rotate(Vector::new(0.0, 1.0, 0.0), PI)
        .rotate(Vector::new(1.0, 0.0, 0.0), PI / 2.0)
        .translate_y(layout::TABLE_HEIGHT)
}
