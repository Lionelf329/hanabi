use super::logic::{state, State};
use crate::constants::{color::CANVAS_COLOR, CANVAS_FACTOR, CANVAS_ID};
use crate::scene::Scene;
use crate::wasm::{dimensions, Canvas};
pub use game::table_card_transform;

mod game;
mod host;
mod initial_load;
mod join;
mod joined;
mod render_card;

pub mod transform;

#[derive(Copy, Clone)]
pub enum Category {
    OwnCard(usize),
    Table,
    Discard,
}

pub static mut SCENE: Option<Scene<Category>> = None;

pub fn draw() {
    let (w, h) = dimensions();
    let s: &State = state();
    let scene = match s {
        State::InitialLoad => initial_load::draw_initial_load(w, h),
        State::Host => host::draw_host(w, h),
        State::Join(code) => join::draw_join(w, h, code),
        State::Joined { code, players, me } => joined::draw_joined(w, h, code, players, *me),
        State::Game { camera, game } => game::draw_game(w, h, camera, game),
    };
    let canvas = get_canvas(w, h);
    scene.draw(&canvas);
    unsafe {
        SCENE = Some(scene);
    };
}

fn get_canvas(w: u32, h: u32) -> Canvas {
    let w = w as f64;
    let h = h as f64;
    let canvas = Canvas::from_id(CANVAS_ID).unwrap();
    canvas.set_width((w * CANVAS_FACTOR) as u32);
    canvas.set_height((h * CANVAS_FACTOR) as u32);
    canvas.scale(CANVAS_FACTOR, CANVAS_FACTOR).unwrap();
    canvas.set_fill_style(CANVAS_COLOR);
    canvas.fill_rect(0.0, 0.0, w, h);
    canvas
}
