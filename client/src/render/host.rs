use crate::constants::{color, svg};
use crate::logic::actions;
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::transform::translate_x;
use graphics::Matrix;

pub fn draw_host<T: Copy>(w: u32, h: u32) -> Scene<T> {
    let mut scene = Scene::new(None, None);
    for i in 2..=5 {
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::CARD[i], color::HOST),
            t: host_transform(w, h, i),
            use_camera: false,
            on_click: actions::host_new(i),
            category: None,
        });
    }
    scene
}

fn host_transform(w: u32, h: u32, i: usize) -> Matrix {
    let factor = w as f64 / 4.0;
    translate_x(svg::SIZE * (i - 2) as f64)
        .scale_all(factor / svg::SIZE)
        .translate_y((h as f64 - factor) / 2.0)
}
