import init, * as module  from './pkg/hanabi_client.js';

async function run() {
    await init();
    window.rs = module;
}

run();
