use super::count_false;
use crate::constants::STACK_SIZE;

#[derive(Copy, Clone, Debug)]
pub enum NumberHintStatus {
    /// No hits yet
    SomeEliminated([bool; STACK_SIZE]),
    /// At least one hit
    Known,
}

impl std::fmt::Display for NumberHintStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            NumberHintStatus::Known => f.write_str("X"),
            NumberHintStatus::SomeEliminated(x) => {
                for i in x.iter() {
                    f.write_str(if *i { "1" } else { "0" }).unwrap();
                }
                Ok(())
            }
        }
    }
}

impl From<&str> for NumberHintStatus {
    fn from(str: &str) -> Self {
        if str == "X" {
            NumberHintStatus::Known
        } else {
            let chars: Vec<char> = str.chars().collect();
            let mut colors = [false; 5];
            for i in 0..5 {
                colors[i] = chars[i] == '1';
            }
            NumberHintStatus::SomeEliminated(colors)
        }
    }
}

impl Default for NumberHintStatus {
    fn default() -> Self {
        NumberHintStatus::SomeEliminated([false; STACK_SIZE])
    }
}

impl NumberHintStatus {
    pub fn hint(&mut self, number: usize, hit: bool) {
        if let NumberHintStatus::SomeEliminated(eliminated) = self {
            if hit {
                // if the hint applies to this card, you know its number
                *self = NumberHintStatus::Known;
            } else {
                // if the hint does not apply, eliminate that number as a possibility
                eliminated[number - 1] = true;

                // if all the other numbers have been hinted, you now know what it is
                if count_false(eliminated) == 1 {
                    *self = NumberHintStatus::Known;
                }
            }
        }
    }

    pub fn is_known(&self) -> bool {
        match self {
            NumberHintStatus::Known => true,
            NumberHintStatus::SomeEliminated(_) => false,
        }
    }
}
