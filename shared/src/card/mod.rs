use crate::constants::{COLORS_WITH_RAINBOW, STACK_SIZE};
pub use color_hint::ColorHintStatus;
pub use number_hint::NumberHintStatus;

mod color_hint;
mod number_hint;

#[derive(Copy, Clone, Debug, Default)]
pub struct Card {
    pub color: usize,
    pub number: usize,
    pub hint_color: ColorHintStatus,
    pub hint_number: NumberHintStatus,
}

impl std::fmt::Display for Card {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&format!(
            "{}-{}-{}-{}",
            self.color, self.number, self.hint_color, self.hint_number
        ))
    }
}

impl From<&str> for Card {
    fn from(str: &str) -> Self {
        let str: Vec<&str> = str.split("-").collect();
        Card {
            color: str[0].parse::<usize>().unwrap(),
            number: str[1].parse::<usize>().unwrap(),
            hint_color: ColorHintStatus::from(str[2]),
            hint_number: NumberHintStatus::from(str[3]),
        }
    }
}

impl Card {
    pub fn new(color: usize, number: usize) -> Card {
        if number > STACK_SIZE || color > 6 {
            panic!("Invalid number or color");
        }
        Card {
            color,
            number,
            hint_color: ColorHintStatus::default(),
            hint_number: NumberHintStatus::default(),
        }
    }

    /// checks whether the hint applies to this card
    pub fn check_hint(&self, hint: Hint) -> bool {
        match hint {
            Hint::Color(color) => self.color == color || self.color == COLORS_WITH_RAINBOW,
            Hint::Number(number) => self.number == number,
        }
    }

    /// updates the player's knowledge of this card, based on this hint
    pub fn hint(&mut self, hint: Hint) {
        let hit = self.check_hint(hint);
        match hint {
            Hint::Color(color) => self.hint_color.hint(color, hit),
            Hint::Number(number) => self.hint_number.hint(number, hit),
        };
    }
}

#[derive(Copy, Clone)]
pub enum Hint {
    Color(usize),
    Number(usize),
}

/// counts how many "true" values are in a sliced array of booleans
fn count_true(values: &[bool]) -> usize {
    values.iter().filter(|&x| *x).count()
}

/// counts how many "false" values are in a sliced array of booleans
fn count_false(values: &[bool]) -> usize {
    values.iter().filter(|&x| !*x).count()
}
