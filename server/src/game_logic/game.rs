use hanabi_shared::constants::{MAX_HINTS, STACK_SIZE};
use hanabi_shared::{Card, Hint};
use std::collections::HashSet;

#[derive(Clone, Debug)]
pub struct Game {
    pub users: HashSet<usize>,
    pub remaining: Vec<Card>,
    pub players: Vec<Player>,
    pub discarded: Vec<Card>,
    pub stacks: Vec<usize>,
    pub hints: usize,
    pub fuses: usize,
    pub turn: usize,
}

#[derive(Clone, Debug)]
pub struct Player {
    pub user_id: Option<usize>,
    pub cards: Vec<Card>,
}

impl Game {
    pub fn play(&mut self, player_index: usize, card_index: usize) -> Result<Option<Card>, ()> {
        // make sure the game hasn't ended, the right player is playing, and the card exists
        self.check_valid_move(player_index, card_index)?;

        // remove the card from the player's hand
        let card = self.players[player_index].cards.remove(card_index);

        // if the card goes onto the stack
        if card.number == self.stacks[card.color - 1] + 1 {
            // increment the stack height
            self.stacks[card.color - 1] += 1;
            // if the stack was just completed, add a hint
            if self.stacks[card.color - 1] == STACK_SIZE {
                self.add_hint();
            }
        } else {
            // move the card to the discard pile and burn a fuse
            self.discarded.push(card);
            self.burn_fuse();
        }

        // draw a new card, and end the turn
        let new_card = self.draw_card(player_index);
        self.next_turn();
        Ok(new_card)
    }

    pub fn discard(&mut self, player_index: usize, card_index: usize) -> Result<Option<Card>, ()> {
        // make sure the game hasn't ended, the right player is playing, and the card exists
        self.check_valid_move(player_index, card_index)?;

        // remove the card from the player's hand
        let card = self.players[player_index].cards.remove(card_index);

        // move the card to the discard pile and add a hint
        self.discarded.push(card);
        self.add_hint();

        // draw a new card, and end the turn
        let new_card = self.draw_card(player_index);
        self.next_turn();
        Ok(new_card)
    }

    pub fn hint(&mut self, player_index: usize, target_index: usize, hint: Hint) -> Result<(), ()> {
        // make sure the game hasn't ended, the right player is playing,
        // they are hinting a different player, and there is a hint available
        self.check_valid_hint(player_index, target_index)?;

        // assume the hint is invalid, and then check if it applies to any cards
        let mut valid = false;
        for card in &self.players[target_index].cards {
            valid |= card.check_hint(hint);
        }

        if valid {
            // if it does apply, then apply it to all relevant cards
            for card in &mut self.players[target_index].cards {
                card.hint(hint);
            }

            // use a hint, then end the turn
            self.use_hint();
            self.next_turn();
            Ok(())
        } else {
            Err(())
        }
    }

    fn check_valid_move(&self, player_index: usize, card_index: usize) -> Result<(), ()> {
        // check if the player playing is the player whose turn it is
        let correct_player = player_index == self.turn;

        // check that the card they are playing/discarding is in their hand
        let valid_card = card_index < self.players[self.turn].cards.len();

        // check if the game is still going, and the above conditions are met
        if self.not_finished() && correct_player && valid_card {
            Ok(())
        } else {
            Err(())
        }
    }

    fn check_valid_hint(&self, player_index: usize, target_index: usize) -> Result<(), ()> {
        // check if the player playing is the player whose turn it is
        let correct_player = player_index == self.turn;

        // check that the player being targeted exists and is not the same as the player
        let valid_target = target_index < self.players.len() && target_index != self.turn;

        // check that there's a hint available
        let hints_remaining = self.hints > 0;

        // check if the game is still going, and the above conditions are met
        if self.not_finished() && correct_player && valid_target && hints_remaining {
            Ok(())
        } else {
            Err(())
        }
    }

    fn not_finished(&self) -> bool {
        // check that there is at least one stack that isn't complete
        let not_won = self.stacks.iter().any(|&x| x != STACK_SIZE);

        // check that there is at least one fuse left
        let not_lost = self.fuses > 0;

        not_won && not_lost
    }

    fn use_hint(&mut self) {
        if self.hints > 0 {
            self.hints -= 1;
        }
    }

    fn add_hint(&mut self) {
        if self.hints < MAX_HINTS {
            self.hints += 1;
        }
    }

    fn burn_fuse(&mut self) {
        if self.fuses > 0 {
            self.fuses -= 1;
        }
    }

    fn next_turn(&mut self) {
        self.turn = (self.turn + 1) % self.players.len();
    }

    fn draw_card(&mut self, player_index: usize) -> Option<Card> {
        // try to remove a card from the deck
        let card = self.remaining.pop();

        // if there was a card, add it to the specified player's hand
        if let Some(card) = card {
            self.players[player_index].cards.push(card);
        }

        card
    }
}
