use crate::game_logic::Game;
use std::collections::HashMap;
use std::time::Instant;
use tokio::sync::mpsc::UnboundedSender;
use warp::ws::Message;
use warp::Error;

pub struct State {
    pub users: HashMap<usize, (Sender, GameJoinStatus, Instant)>,
    pub games: HashMap<usize, Game>,
    pub last_cleanup: Instant,
}

impl Default for State {
    fn default() -> Self {
        State {
            users: HashMap::default(),
            games: HashMap::default(),
            last_cleanup: Instant::now(),
        }
    }
}

pub type Sender = UnboundedSender<Result<Message, Error>>;

#[derive(Copy, Clone, Debug)]
pub enum GameJoinStatus {
    NotJoined,
    Spectating(usize),
    Joined(usize, usize),
}

impl State {
    pub fn add_user(&mut self, id: usize, sender: Sender) {
        self.users
            .insert(id, (sender, GameJoinStatus::NotJoined, Instant::now()));
        self.send(id, &format!("{}", id));
    }

    pub fn remove_user(&mut self, user_id: usize) {
        if let GameJoinStatus::Joined(game_id, i) = self.users[&user_id].1 {
            // If they are part of a game, remove them and notify the other players
            if let Some(game) = self.games.get_mut(&game_id) {
                game.players[i].user_id = None;
                game.users.remove(&user_id);
            }
            self.broadcast(game_id, &format!("leave {}", i));
        } else if let GameJoinStatus::Spectating(game_id) = self.users[&user_id].1 {
            // If they are spectating a game, unsubscribe them from updates
            if let Some(game) = self.games.get_mut(&game_id) {
                game.users.remove(&user_id);
            }
        }
        self.users.remove(&user_id);
    }

    pub fn send(&mut self, id: usize, message: &str) {
        if let Some((x, ..)) = self.users.get(&id) {
            if x.send(Ok(Message::text(message))).is_err() {
                self.remove_user(id);
            }
        }
    }

    pub fn broadcast(&mut self, game_id: usize, message: &str) {
        let users: Vec<usize> = self.games[&game_id].users.iter().map(|x| *x).collect();
        for id in users {
            self.send(id, message);
        }
    }
}
