use crate::constants::{ADDRESS, PORT, STATIC_DIR};
use crate::game_logic::State;
use hanabi_shared::constants::WEBSOCKET_ROUTE;
use main_ws::on_connect;
use std::sync::Arc;
use tokio::sync::Mutex;
use warp::ws::Ws;
use warp::Filter;

mod constants;
mod game_logic;
mod main_ws;
mod websocket;

type ArcState = Arc<Mutex<State>>;

#[tokio::main]
pub async fn main() {
    // initialize state
    let state: ArcState = Arc::new(Mutex::new(State::default()));
    // convert state to warp filter
    let state = warp::any().map(move || Arc::clone(&state));

    // create warp filter for serving static files
    let route1 = warp::fs::dir(STATIC_DIR);
    // create warp filter for websocket
    let route2 = warp::path(WEBSOCKET_ROUTE)
        .and(warp::ws())
        .and(state)
        .map(|ws: Ws, state| ws.on_upgrade(|websocket| on_connect(websocket, state)));
    // run server on configured address
    warp::serve(route1.or(route2)).run((ADDRESS, PORT)).await;
}
