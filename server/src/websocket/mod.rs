use crate::ArcState;
use parse_action::{parse, WsAction};
use std::time::Instant;

mod actions;
mod parse_action;

pub async fn on_message(user_id: usize, message: &str, state: &ArcState) {
    if message == "" {
        ping(user_id, state).await;
        return;
    }
    let action = match parse(message) {
        Some(action) => action,
        None => return,
    };
    match action {
        WsAction::Discard(card_index) => actions::discard(user_id, state, card_index).await,
        WsAction::Hint(target, hint) => actions::hint(user_id, state, target, hint, message).await,
        WsAction::Host(player_count) => actions::host(user_id, state, player_count).await,
        WsAction::Join(position) => actions::join(user_id, state, position).await,
        WsAction::Play(card_index) => actions::play(user_id, state, card_index).await,
        WsAction::Spectate(game_id) => actions::spectate(user_id, state, game_id).await,
    }
}

async fn ping(user_id: usize, state: &ArcState) {
    let mut s = state.lock().await;
    s.users.get_mut(&user_id).unwrap().2 = Instant::now();
    s.send(user_id, "");
    if s.last_cleanup.elapsed().as_millis() > 1000 {
        s.last_cleanup = Instant::now();
        let users: Vec<usize> = s.users.keys().map(|x| *x).collect();
        for id in users {
            if s.users[&id].2.elapsed().as_millis() > 2000 {
                s.remove_user(id);
            }
        }
    }
}
