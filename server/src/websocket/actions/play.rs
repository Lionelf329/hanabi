use crate::game_logic::GameJoinStatus;
use crate::ArcState;

pub async fn play(user_id: usize, state: &ArcState, card_index: usize) {
    let mut s = state.lock().await;
    if let GameJoinStatus::Joined(game_id, player) = s.users[&user_id].1 {
        let game = s.games.get_mut(&game_id).unwrap();
        let outcome = game.play(player, card_index);
        if let Ok(new_card) = outcome {
            let message = &match new_card {
                Some(card) => format!("play {} draw {}", card_index, card),
                None => format!("play {}", card_index),
            };
            s.broadcast(game_id, &message);
        }
    }
}
