use crate::game_logic::{GameJoinStatus, State};
use crate::ArcState;

pub async fn join(user_id: usize, state: &ArcState, position: usize) {
    let mut s = state.lock().await;
    let status = s.users[&user_id].1;
    let game_id = match status {
        GameJoinStatus::Spectating(c) => c,
        GameJoinStatus::Joined(c, _) => c,
        _ => return,
    };
    if s.games[&game_id].players[position].user_id.is_some() {
        return;
    }
    leave_game(&mut s, user_id);
    join_game(&mut s, user_id, game_id, position);
    let game = &s.games[&game_id];
    if game.players.iter().all(|x| x.user_id.is_some()) {
        start_game(&mut s, game_id);
    }
}

fn leave_game(s: &mut State, user_id: usize) -> Option<()> {
    if let GameJoinStatus::Joined(game_id, i) = s.users[&user_id].1 {
        s.users.get_mut(&user_id)?.1 = GameJoinStatus::Spectating(game_id);
        s.games.get_mut(&game_id)?.players[i].user_id = None;
        s.broadcast(game_id, &format!("leave {}", i));
    }
    Some(())
}

fn join_game(s: &mut State, user_id: usize, game_id: usize, position: usize) -> Option<()> {
    s.users.get_mut(&user_id)?.1 = GameJoinStatus::Joined(game_id, position);
    let game = s.games.get_mut(&game_id).unwrap();
    game.players.get_mut(position).unwrap().user_id = Some(user_id);
    let users: Vec<usize> = s.games[&game_id].users.iter().map(|x| *x).collect();
    for id in users {
        let message = if id == user_id {
            format!("join {}", position)
        } else {
            format!("join_other {}", position)
        };
        s.send(id, &message);
    }
    Some(())
}

fn start_game(s: &mut State, game_id: usize) -> Option<()> {
    let game = &s.games[&game_id];
    let mut message = format!(
        "start_game {} {} {} {}",
        game.hints,
        game.fuses,
        game.turn,
        game.remaining.len()
    );
    for s in &game.stacks {
        message += &format!(" {}", s);
    }
    for p in &game.players {
        message += " |";
        for card in &p.cards {
            message += &format!(" {}", card);
        }
    }
    message += " x";
    for card in &game.discarded {
        message += &format!(" {}", card);
    }
    s.broadcast(game_id, &message);
    Some(())
}
