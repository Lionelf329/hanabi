use crate::game_logic::{Game, GameJoinStatus};
use crate::ArcState;
use std::collections::HashMap;

pub async fn spectate(user_id: usize, state: &ArcState, game_id: usize) {
    let mut s = state.lock().await;
    if let Some(game) = s.games.get_mut(&game_id) {
        game.users.insert(user_id);
        if let Some(user) = s.users.get_mut(&user_id) {
            // todo: this won't work if they are already part of a game, but that's currently impossible
            user.1 = GameJoinStatus::Spectating(game_id);
        }
        let message = spectate_message(&s.games, game_id);
        s.send(user_id, &message);
    }
}

fn spectate_message(games: &HashMap<usize, Game>, game_id: usize) -> String {
    let mut msg = format!("spectate {}", game_id);
    for p in &games[&game_id].players {
        if p.user_id.is_some() {
            msg += " 1";
        } else {
            msg += " 0";
        }
    }
    msg
}
