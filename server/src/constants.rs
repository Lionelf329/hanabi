// Server config
pub const STATIC_DIR: &str = "client/static";
pub const ADDRESS: [u8; 4] = [127, 0, 0, 1];
pub const PORT: u16 = 3000;

// Initial game values
pub const RAINBOW: bool = true;

pub fn cards_per_player(player_count: usize) -> usize {
    match player_count {
        2 => 5,
        3 => 5,
        4 => 4,
        5 => 4,
        _ => panic!(),
    }
}
