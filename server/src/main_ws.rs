use crate::websocket::on_message;
use crate::ArcState;
use futures::{FutureExt, StreamExt};
use std::sync::atomic::{AtomicUsize, Ordering};
use tokio::{sync::mpsc, task::spawn};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::WebSocket;

static NEXT_USER_ID: AtomicUsize = AtomicUsize::new(1);

pub async fn on_connect(ws: WebSocket, state: ArcState) {
    // get the next unique id
    let user_id = NEXT_USER_ID.fetch_add(1, Ordering::Relaxed);

    // Split the socket into a sender and receive of messages
    let (ws_sender, mut ws_receiver) = ws.split();

    // Use an unbounded channel to handle buffering and flushing of messages to the websocket
    let (sender, receiver) = mpsc::unbounded_channel();
    let receiver_stream = UnboundedReceiverStream::new(receiver);
    spawn(receiver_stream.forward(ws_sender).map(move |result| {
        if let Err(error) = result {
            println!("websocket send error (uid={}): {}", user_id, error);
        }
    }));

    state.lock().await.add_user(user_id, sender);

    while let Some(result) = ws_receiver.next().await {
        if !state.lock().await.users.contains_key(&user_id) {
            return;
        }
        match result {
            Ok(message) => {
                // ignore non-text messages
                if let Ok(message) = message.to_str() {
                    on_message(user_id, message, &state).await;
                }
            }
            Err(error) => {
                println!("websocket receive error (uid={}): {}", user_id, error);
                break;
            }
        }
    }

    state.lock().await.remove_user(user_id);
}
