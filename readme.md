# Setup Instructions

### Install Rust and wasm-pack
This project was entirely written in rust, and compiled
using wasm-pack and cargo. You can download the tools
you'll need here:

- https://www.rust-lang.org/tools/install
- https://rustwasm.github.io/wasm-pack/

As always, make sure you add things to your PATH when
you install them, or the commands won't work.

### Cloning the repositories
#### SSH
```
git clone git@gitlab.com:lionelf329/graphics.git
git clone git@gitlab.com:lionelf329/hanabi.git
```
#### HTTPS
```
git clone https://gitlab.com/lionelf329/graphics.git
git clone https://gitlab.com/lionelf329/hanabi.git
```

### Building and Running
#### Client
```
wasm-pack --log-level error build client --target web --out-dir static\pkg
```
#### Server
```
cargo run server
```

# Starting a game
### ![image](_docs/host.svg) Host a game
After clicking the green icon on the first page, you will be
asked to choose a number of players. Once you have selected
a number, you will be taken to the lobby, where you will be
given a code, which you should share with the other players.
### ![image](_docs/join.svg) Join a game
After clicking the blue icon on the first page, you will need
to enter the code given to you by the host, which you can do
by clicking the blue boxes with numbers in them. If you make
a mistake, you can delete individual digits by clicking the
grey boxes. Once a valid code has been entered, you will be
transferred to the lobby.
### Lobby
Here, you can see the game code and the arrangement of players
around the table. The colors of the player icons have the
following meanings:

| Icon                        | Meaning      |
|-----------------------------|--------------|
| ![image](_docs/person3.svg) | You          |
| ![image](_docs/person2.svg) | Other player |
| ![image](_docs/person1.svg) | Empty        |

Click on an empty seat to join. Once all seats have been
filled, the game will start.

# Rules of the game
### Objective
The object of the game is to complete one firework of each color,
by playing each of the numbered cards in ascending order. Each
player can see each other players' cards but not their own, so
everyone must work together to ensure that the right cards are
played. On each player's turn, they have up to three options.
They can play a card, discard a card, or give another player a hint.
The rules for how this works are as follows.

### Play

If the card you are playing is the next one of its color, then it
goes on the table in the appropriate location. If you complete a
firework, you get a hint back.

Otherwise, the card is discarded, and a fuse is burned. If three
fuses are burned, the game ends and everyone loses.

After playing a card, the player will draw another card from the
deck, if there is one.

### Discard

The player moves the card to the discard pile, draws another from
the deck, and gets a hint back.

### Hint

This costs one hint, so it cannot be done if there are no hints
remaining. A hint
must be targeted towards exactly one player, and is always phrased
as "This/these card(s) are _____", where the blank is a color or
a number. A hint must refer to at least one card (You can't
point to zero cards), and you can't hint rainbow explicitly,
because rainbow is a special color that, for the purpose of
hints, is every color.

# How to Play
### How to find information about the game
- The number of fuses burned is in the top left.
- The number of hints available is in the top right.
- The number of cards remaining in the deck is in the bottom left.
- The cards that have been discarded are visible in the bottom right.
- The progress through each firework is on the table.
- The player whose turn it is is highlighted cyan. If nobody else
is highlighted, it's your turn.
- If there is a location marker above a card, that means a hint was
made that refers to that card. If the location marker is colored,
it was a color hint, if it is grey, then it was a number hint.
- If one of your cards has a number, it is definitely that number.
- If one of your cards is brightly colored, it is definitely that
color.
- If one of your cards is colored but faded, it is either that color
or rainbow.
- If you hold down on a card, you can see along the top what hints
have been given while holding that card that didn't refer to it.

### How to make moves

- To play a card, drag it from your hand to the table.
- To discard a card, drag it from your hand to the discard pile.
- To give a hint, zoom in on a player by clicking on or near
their cards, and then choose the hint by clicking one of the
icons that appear near the top of the screen.

# Tips
- There are three 1s, two 2s, two 3s, two 4s, and one 5 of each
color. Pay attention to how many of each have been discarded,
or you may make it impossible to complete a firework.
- Cards are drawn on the left side of a player's hand, and
should therefore be discarded on the player's right, so that
other players have as much chance as possible to hint them if
they are important.
- Hints are a limited resource, so they should always give the
player information that allows them to play, discard, or both.
As a corollary to that, when receiving a hint, assume that
there is a lot of subtext about why they would give that hint
at that particular time.
- All else being equal, newer cards are more likely to be
useful than older cards, because other players haven't had a
chance to hint them yet.
- Using the last hint is itself a piece of information,
because it means that the next player won't need a hint.
- Never discard a card that you know nothing about if you
can give a hint instead, even if it's not a good hint.